const fs = require('fs');
const file = './weather.txt';
const fields = ['date', 'temperature', 'precipitation', 'pressure', 'winddirection', 'windspeed'];
fs.readFile(file, 'utf8', (err, data) => {
  if (err) throw err;
  data = data.trim().split('\n')
      .map(line => line.split('\t'))
      .map(d => {
        let obj = {};
        for (let i = 0; i < fields.length; i += 1)
          obj[fields[i]] = isNaN(d[i].replace(',', '.'))? d[i].trim() : Number(d[i].replace(',', '.'));
        return obj;
      });
  console.log(data);
  let headers = data.shift();
  fs.writeFile('./weather.json', JSON.stringify({headers, data}, null, 2), console.error)
});
