
$(function() {

  var tileSquare = 20 * 30;

  function update() {
    var h = Number($("#heightSlider").slider('value'));
    var w = Number($("#widthSlider").slider('value'));
    $('#height').text(h);
    $('#width').text(w);
    var wallSquare = w * h;
    var tileCount = Math.ceil(wallSquare / tileSquare)
    $('#S').text(tileCount);
  }

  $("#heightSlider").slider({
    orientation: 'vertical',
    max: 1000,
    slide: update
  });

  $("#widthSlider").slider({
    max: 1000,
    slide: update
  });

});
