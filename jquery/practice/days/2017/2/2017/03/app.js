
$(main);

function main() {
  $('button.sendForm').on('click', test);
  $('[type=submit]').on('click', test);
  $('input[name=fio]').on('input', function() {
    var oldValue = $(this).val();
    var newValue = oldValue.replace(/[-.,$+'"]/ig, '');
    $(this).val(newValue);
  });
}

function test(event) {
  event.preventDefault();

  var element = document.getElementById('out');
  /*
  | HTML          |  DOM (JS)                                  | jQuery
  | <div id=out>  |  document.getElementById('out').innerHTML  | $('#out').html()
  |               |  document.querySelector('#out').innerHTML  |
  */



  var out = $('#out');
  out.html(
      'ФИО: '      + $('#form [name=fio]').val()           + '<br>' +
      'Пол: '      + $('#form [name=gender]:checked').val() + '<br>' +
      'Класс: '    + $('#form [name=class]').val()          + '<br>' +
      'Описание: ' + $('#form [name=description]').val()
  );
}
