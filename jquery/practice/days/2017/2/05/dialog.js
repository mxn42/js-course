function showDialog() {
  $('[data-role="popup-dialog"]').addClass('active');
}

function init() {
  $('[data-role="popup"]').on('click', showDialog);

  $('.dialog button.closeButton').on('click', function() {
    $('[data-role="popup-dialog"]').removeClass('active');
  });
}
/*
document.addEventListener('DOMContentLoaded', function() {
  init();
});

$(document).on('ready', function() {
  init();
});

$(document).ready(function() {
  init();
});

$(function() {
  init();
});
*/
$(init);
