$(main);

// Загрузка по нажатию на кнопку
function main() {
  $('button').on('click', function() {
    $.getJSON('elements.json', filteredShow);
  });
}

function filteredShow(elements) {
  var min = Number($('input[name=min]').val());
  // var max = ...

  var filtered = elements.filter(function(elem) {
    //return (min < elem.atomic_mass) && (elem.atomic_mass < ...);
  });

  show(filtered);
}

function show(elements) {
  var tbody = $('#box tbody');
  tbody.empty();
  elements.forEach(function (item) {
    var tr = $('<tr>');
    tr.append($('<td>').text(item.number));
    tr.append($('<td>').text(item.symbol));
    tr.append($('<td>').text(item.name));
    tr.append($('<td>').text(item.atomic_mass));
    tr.append($('<td>').text(item.source));
    tbody.append(tr);
  });
}
