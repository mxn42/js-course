$(main);

function main() {
  $('#add').on('click', function() {
    $('#dialog').dialog({
      height: 400,
      width: 350,
      modal: true,
      buttons: {
        "Add": function() {
          $('#dialog').dialog("close");
          addOrder();
        },
        "Cancel": function() {
          $('#dialog').dialog("close");
        }
      }
    });
  });
}

function addOrder() {
  var number = $('input[name=number]').val();
  var name = $('input[name=name]').val();
  var price = $('input[name=price]').val();

  var tbody = $('#box tbody');
  var tr = $('<tr>');
  tr.append($('<td>').text(number));
  tr.append($('<td>').text(name));
  tr.append($('<td>').text(price));
  tbody.append(tr);
}

