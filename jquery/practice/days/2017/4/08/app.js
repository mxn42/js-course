$(main);

function main() {
  $('#range').slider({
    range: true,
    min: 0,
    max: 300,
    values: [0, 300],
    slide: function( event, ui ) {
      $.getJSON('elements.json', filteredShow);
    }
  });

  $.getJSON('elements.json', filteredShow);
}

function filteredShow(elements) {
  var min = $('#range').slider('values', 0);
  var max = $('#range').slider('values', 1);

  var filtered = elements.filter(function(elem) {
    return (min < elem.atomic_mass) && (elem.atomic_mass < max);
  });

  show(filtered);
}

function show(elements) {
  var tbody = $('#box tbody');
  tbody.empty();
  elements.forEach(function (item) {
    var tr = $('<tr>');
    tr.append($('<td>').text(item.number));
    tr.append($('<td>').text(item.symbol));
    tr.append($('<td>').text(item.name));
    tr.append($('<td>').text(item.atomic_mass));
    tr.append($('<td>').text(item.source));
    tbody.append(tr);
  });
}
