
$(main);

function main() {
  $('button').on('click', calc);
  $('[name=a]').slider();
  $('[name=b]').slider({
    orientation: "vertical",
    min: 0,
    max: 100,
    value: 60,
    slide: function(event, ui) {
      calc();
    }
  });
}

function calc() {
  var a = $('[name=a]').slider('value');
  var b = $('[name=b]').slider('value');
  var o = $('select[name=operation]').val();

  var c;
  if (o === '+') c = a + b;
  else if (o === '-') c = a - b;
  else if (o === '*') c = a * b;
  else if (o === '/') c = a / b;
  else c = 'Oups!';

  $('.sum').text(c);

  $('button')
      .prop('disabled', true)
      .text('ВЫКЛЮЧЕНА')
      .off('click');

  setTimeout(function () {
    $('button')
        .prop('disabled', false)
        .text('Вычислить')
        .on('click', calc);
  }, 4000);
}
