
$(main);

function main() {
  console.log('Hello, world');
  $('button').on('click', sum);
}

function sum() {
  var a = Number($('input[name=a]').val());
  var b = Number($('input[name=b]').val());
  var c = a + b;
  $('.sum').text(c);

  $('button')
      .prop('disabled', true)
      .text('ВЫКЛЮЧЕНА')
      .off('click');

  setTimeout(function () {
    $('button')
        .prop('disabled', false)
        .text('Вычислить')
        .on('click', sum);
  }, 4000);
}


