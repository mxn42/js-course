
$(main);

function main() {
  console.log('Hello, world');
  $('button').on('click', sum);
}

function sum() {
  var a = Number($('input[name=a]').val());
  var b = Number($('input[name=b]').val());
  var c = a + b;
  $('.sum').text(c);

  $('button').prop('disabled', true);
  $('button').off('click');

  setTimeout(buttonOn, 4000);
}

function buttonOn() {
  $('button').prop('disabled', false);
  $('button').on('click', sum);
}
