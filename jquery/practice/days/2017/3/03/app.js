
$(main);

function main() {
  $('input[name=a]').on('change', calc);
  $('input[name=b]').on('change', calc);
  $('select[name=operation]').on('change', calc);
}

function calc() {
  var a = Number($('input[name=a]').val());
  var b = Number($('input[name=b]').val());
  var o = $('select[name=operation]').val();

  var c = (o === '+')? a + b :
          (o === '-')? a - b :
          (o === '*')? a * b :
          (o === '/')? a / b :
          'Oups!';

  $('.sum').text(c);
}


