
$(main);

function main() {
  console.log('Hello, world');
  $('button').on('click', calc);
}

var ops = {};
ops['+'] = function(a, b) { return a + b; };
ops['-'] = function(a, b) { return a - b; };
ops['*'] = function(a, b) { return a * b; };
ops['/'] = function(a, b) { return a / b; };

// var ops = {
//   '+': (a, b) => a + b,
//   '-': (a, b) => a - b,
//   '*': (a, b) => a * b,
//   '/': (a, b) => a / b
// };

function calc() {
  var a = Number($('input[name=a]').val());
  var b = Number($('input[name=b]').val());
  var o = $('select[name=operation]').val();

  // var c;
  // if (o === '+') c = a + b;
  // else if (o === '-') c = a - b;
  // else if (o === '*') c = a * b;
  // else if (o === '/') c = a / b;
  // else c = 'Oups!';

  // var c = o === '+'? a + b :
  //         o === '-'? a - b :
  //         o === '*'? a * b :
  //         o === '/'? a / b :
  //         'Oups!';

  var c = ops[o]? ops[o](a, b) : 'Oops!';

  $('.sum').text(c);

  $('button')
      .prop('disabled', true)
      .text('ВЫКЛЮЧЕНА')
      .off('click');

  setTimeout(function () {
    $('button')
        .prop('disabled', false)
        .text('Вычислить')
        .on('click', calc);
  }, 4000);
}
