
$(main);

function main() {
  var students = people;
  students.sort(function(a, b) {
    return a.age - b.age;
  });
  var person = students[0];

  var info = $('#student-info');
  info.find('.last-name').text(person.name.last);
  info.find('.first-name').text(person.name.first);
  info.find('.group').text(person.group);
  info.find('.age').text(person.age);
  info.find('.gender').text({m:'M',f:'Ж'}[person.gender]);
}
