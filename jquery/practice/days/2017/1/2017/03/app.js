
$(main);

function main() {
  var students = people;

  students.sort(function(a, b) {
    return a.group - b.group ||
           a.name.last.localeCompare(b.name.last) ||
           a.name.first.localeCompare(b.name.first) ||
           a.age - b.age;
  });

  studentTable(students);
}

function studentTable(students) {
  var tbody = $('#table-list tbody');
  tbody.empty();

  students.forEach(function(person) {
    var tr = $('<tr>');
    tr.append($('<td>').text(person.name.last));
    tr.append($('<td>').text(person.name.first));
    tr.append($('<td>').text(person.group));
    tbody.append(tr);
  });
}
