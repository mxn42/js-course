
document.addEventListener('DOMContentLoaded', main);

function main() {
  var students = people;

  students.sort(function(a, b) {
    return a.age - b.age;
  });

  var person = students[0];
  var info = document.getElementById('student-info');
  info.innerHTML = '';
  info.innerHTML += 'Фамилия: ' + person.name.last + '<br>';
  info.innerHTML += 'Имя: ' + person.name.first + '<br>';
  info.innerHTML += 'Группа: ' + person.group + '<br>';
  info.innerHTML += 'Возраст: ' + person.age + '<br>';
  info.innerHTML += 'Пол: ' + (person.gender === 'm'? 'М' : 'Ж');
}
