# jQuery

## Практика 1

### Примеры
```javascript
jQuery(function() {
 console.log('Hello, world');
});

var element = jQuery('#thefirst');
var element = jQuery('.active');
var element = jQuery('input[name="password"]');

var elements = jQuery('p');
var elements = jQuery('#tabl1 td');
var elements = jQuery('ul.menu li');

var text = jQuery('#first').text();
jQuery('#first').text('Новое значение');

var html = jQuery('#first').html();
jQuery('#first').html('<h1>Новое значение</h1>');

var h1Element = jQuery('<h1>Заголовок</h1>');
jQuery('body').append(h1)
```

### Задачи



01. Подключить файл students.js. 
 
 1. Вывести количество студентов.
 2. Вывести таблицу с данными первых двух студентов с полями
       - фамилия
       - имя
       - группа 
       - возраст
       - пол

Используя сортировку,
 3. Вывести информацию о самом младшем студенте в группе.
 4. Вывести информацию о 5 самых младших студентах в группе.














02. Подключить jQuery, исправить п.1 используя jQuery.

03. Таблица со списком студентов, поля: Фамилия, Имя, Группа

04. Дополнить таблицу полями: ID, Возраст, Пол. Отсортировать по 'Группе', 'Фамилии' и по 'Имени'.


### ДЗ

01. На основе примера [03]() реализовать вывод таблицы планет, файл [../data/planets.js].

02*. Просмотреть и попробвать решить задачи 1-6 из файла [tasks.md]().



