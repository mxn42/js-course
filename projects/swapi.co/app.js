$(main);

function main() {
  $('#tabs').tabs({
    activate: function(event, ui) {
      show(ui.newPanel);
    }
  });
}

function show(panel) {
  var resource = panel.attr('data-url');
  if (resource) {
    panel.empty();

    var nav =   $('<nav>').appendTo(panel);
    var url = 'https://swapi.co/api' + resource;
    panel.append($('<a class=url>').text(url).attr({href: url, target: '_blank'}));

    var table = $('<table border=1>').appendTo(panel);
    var thead = $('<thead>').appendTo(table);
    var tbody = $('<tbody>').appendTo(table);

    loadSchema(url, function(schema) {
      showHeader(thead, schema);

      $.getJSON(url, function(data) {
        showBody(tbody, schema, data);
        showNav(nav, resource, schema, tbody, data);
      });
    });
  }
}

function loadSchema(url, fn) {
  $.getJSON(url + '/schema', function(data) {
    fn(data);
  });
}

function showNav(nav, resource, schema, tbody, data) {
  nav.empty();
  nav.append($('<span>').text('Count: ' + data.count + ' '));
  var pages = $('<span> Pages: ').appendTo(nav);
  for (var i = 0; i * 10 < data.count; i += 1) {
    $('<button>')
      .text(i + 1)
      .appendTo(pages)
      .on('click', function() {
        $('button.active').removeClass('active');
        var page = $(this).addClass('active').text();
        var url = 'https://swapi.co/api' + resource + '/?page=' + page;
        $('a.url').text(url);
        $.getJSON(url, function(data) {
          showBody(tbody, schema, data);
        });
      });
  }
  $('button:first').addClass('active');
}

function showHeader(thead, schema) {
  var tr = $('<tr>').appendTo(thead);
  Object.keys(schema.properties).forEach(function(prop) {
    tr.append($('<th>').text(prop));
  });
}

function showBody(tbody, schema, data) {
  tbody.empty();
  var headers = Object.keys(schema.properties);
  data.results.forEach(function(item) {
    var tr = $('<tr>').appendTo(tbody);
    headers.forEach(function(prop) {
      tr.append(cell(schema, prop, item));
    });
  });
}

function cell(schema, prop, item) {
  var td = $('<td>');
  switch (schema.properties[prop].type) {
    case 'array':
      item[prop].forEach(function(value) {
        td.append($('<a>').text(value).attr({href: item[prop], target: '_blank'})).append($('<br>'));
      });
      break;
    case 'string':
      if (schema.properties[prop].format === 'date-time') {
        td.text(new Date(item[prop]).getFullYear());
        break;
      }
      else if (schema.properties[prop].format === 'uri') {
        td.append($('<a>').text(item[prop]).attr({href: item[prop], target: '_blank'}));
        break;
      }
    default:
      td.text(item[prop]);
  }
  return td;
}
