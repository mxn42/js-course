
function groupInfo(name, groupStudents) {
  let males = 0;
  let females = 0;
  let sum = 0;
  for (let person of groupStudents) {
    sum += person.age;
    if (person.gender === 'm') males += 1;
    if (person.gender === 'f') females += 1;
  }
  let res = 'Группа: ' + name + '<br>';
  res += 'Число студентов: ' + groupStudents.length + '<br>';
  res += 'Средний возраст: ' + (sum / groupStudents.length).toFixed(1) + '<br>';
  res += 'Мужчин: ' + males + '<br>';
  res += 'Женщин: ' + females + '<br>';
  return res;
}

let groups = [];
for (let person of students) {
  if (!groups.includes(person.group))
    groups.push(person.group);
}

for (let group of groups) {
  let groupStudents = students.filter(person => person.group === group);
  let stat = groupInfo(group, groupStudents);
  document.getElementById('output').innerHTML += stat + '<br>';
}
