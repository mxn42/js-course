
let a = [ "!", true, 1 , 5 , -10 ];
a[3]  // 5
a[0]  // "!"

a[2] = 44

let student = {
  name: "Vasya Oblomov",
  group: 24,
  gender: "M"
};

student['gender']   // "var"
student.name      // "var"

student.name = "Olya Petrova";
student['name'] = "Olya Petrova";
