
document.addEventListener('DOMContentLoaded', main);

function main() {
  nav();
  window.addEventListener('hashchange', nav);
}

function nav() {
  let path = (location.hash || '#').substr(1) || 'main';
  document.querySelector('.tab.active').classList.remove('active');
  document.querySelector('.tab.' + path).classList.add('active');
  document.querySelector('.nav .active').classList.remove('active');
  document.querySelector('.nav .' + path).classList.add('active');
}
