
document.addEventListener('DOMContentLoaded', main);

function main() {
  groupsTab(students);
}

function groupsTab(students) {
  let groups = students.reduce((aggr, person) => {
    if (!aggr.includes(person.group))
      aggr.push(person.group);
    return aggr;
  }, []);
  //let groups = Array.from(new Set(students.map(x => x.group)));
  groups.sort((a, b) => a - b);

  let tbody = document.querySelector('.groups.table tbody');
  tbody.innerHTML = '';
  for (let group of groups) {
    let tr = document.createElement('tr');
    tbody.appendChild(tr);
    let td1 = document.createElement('td');
    tr.appendChild(td1);
    td1.textContent = 'Group ' + group;
    let td2 = document.createElement('td');
    tr.appendChild(td2);
    let groupStudents = students.filter(person => person.group === group);
    td2.innerHTML = stat(groupStudents);
  }
}

function stat(students) {
  let malesCount = 0;
  let femalesCount = 0;
  let ageMalesSum = 0;
  let ageFemalesSum = 0;
  for (let person of students) {
    if (person.gender === 'm')  {
      malesCount += 1;
      ageMalesSum += person.age;
    }
    if (person.gender === 'f') {
      femalesCount += 1;
      ageFemalesSum += person.age;
    }
  }
  return `
    число мужчин: ${malesCount} <br>
    число женщин: ${femalesCount} <br>
    средний возраст: ${ ((ageMalesSum + ageFemalesSum) / students.length).toFixed(1) } <br>
    средний возраст мужчин: ${(ageMalesSum / malesCount).toFixed(1)} <br>
    средний возраст женщин: ${(ageFemalesSum / femalesCount).toFixed(1)} 
  `;
}
