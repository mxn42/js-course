
document.addEventListener('DOMContentLoaded', main);

function main() {
  studentsTab(students);
}

const familySort = (a, b) => a.name.last.localeCompare(b.name.last) || a.name.first.localeCompare(b.name.first);
const nameSort =   (a, b) => a.name.first.localeCompare(b.name.first) || a.name.last.localeCompare(b.name.last);
const groupSort =  (a, b) => a.group - b.group || familySort(a, b);
const ageSort =    (a, b) => a.age - b.age || familySort(a, b);
const genderSort = (a, b) => G[a.gender].localeCompare(G[b.gender]) || familySort(a, b);

let lastSortMethod;
function sort(method) {
  if (lastSortMethod === method)
    lastStudents.reverse();
  else
    lastStudents.sort(method);
  lastSortMethod = method;
  showStudents(lastStudents);
}

function getGroups(students) {
  return Array.from(new Set(students.map(x => x.group))).sort();
}

function studentsTab(students) {
  generateGroupButtons(students);
  showStudents(students);
}

function generateGroupButtons(students) {
  let groups = getGroups(students);
  let buttonGroup = document.querySelector('.groupButtons');
  for (let group of groups) {
    let button = document.createElement('button');
    buttonGroup.appendChild(button);
    button.textContent = 'Группа ' + group;
    button.addEventListener('click', function() {
      showGroup(group);
    });
  }
}


let G = { f: 'Ж', m: 'M' };
let lastStudents = [];

function showStudents(students) {
  let tbody = document.querySelector('.students.table tbody');
  tbody.innerHTML = '';
  for (let person of students)
    tbody.innerHTML += `
      <tr>
        <td>${person.name.last}
        <td>${person.name.first}
        <td>${person.group}
        <td>${person.age}
        <td>${G[person.gender]}
    `;
  lastStudents = students;
}

function showGroup(group) {
  if (group)
    showStudents(students.filter(x => x.group === group));
  else
    showStudents(students);
}



// function showStudents(students) {
//   let tbody = document.querySelector('.students.table tbody');
//   tbody.innerHTML = '';
//   for (let person of students)
//     tbody.appendChild(studentTR(person));
// }
//
// function studentTR(person) {
//   let tr = document.createElement('tr');
//   tr.appendChild(td(person.name.last));
//   tr.appendChild(td(person.name.first));
//   tr.appendChild(td(person.group));
//   tr.appendChild(td(person.age));
//   tr.appendChild(td(G[person.gender]));
//   return tr;
// }
//
// function td(text) {
//   let td = document.createElement('td');
//   td.textContent = text;
//   return td;
// }
