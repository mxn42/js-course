
document.addEventListener('DOMContentLoaded', main);

function main() {
  homeTab(students);
}

function homeTab(students) {
  let groups = [];
  let ageSum = 0;
  let malesCount = 0;
  let femalesCount = 0;
  for (let person of students) {
    ageSum += person.age;
    if (person.gender === 'm')
      malesCount += 1;
    if (person.gender === 'f')
      femalesCount += 1;
    if (!groups.includes(person.group))
      groups.push(person.group);
  }
  groups.sort((a, b) => a - b);

  document.querySelector('.allCount.field').textContent = students.length;
  document.querySelector('.malesCount.field').textContent = malesCount;
  document.querySelector('.femalesCount.field').textContent = femalesCount;
  document.querySelector('.groupsCount.field').textContent = groups.length;
  document.querySelector('.groupsAvg.field').textContent = (students.length / groups.length).toFixed(1);
  document.querySelector('.ageAvg.field').textContent = (ageSum / students.length).toFixed(1);
  document.querySelector('.malesAvg.field').textContent = (malesCount / groups.length).toFixed(1);
  document.querySelector('.femalesAvg.field').textContent = (femalesCount / groups.length).toFixed(1);
}
