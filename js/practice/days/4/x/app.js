
window.onload = main;

function main() {
  let studentsCount = students.length;
  let femalesCount = 0;
  let malesCount = 0;
  let agesSum = 0;
  let maxAge = 0;
  let minAge = 200;
  let groups = [];
  for (let person of students) {
  // for (let i = 0; i < students.length; i =+ 1) {
  //   var person = students[i];
    if (person.gender === 'f') femalesCount += 1;
    if (person.gender === 'm') malesCount += 1;
    agesSum += person.age;
    if (person.age > maxAge) maxAge = person.age;
    if (person.age < minAge) minAge = person.age;
    if (!groups.includes(person.group))
      groups.push(person.group);
  }
  let averAge = agesSum / studentsCount;
  document.getElementById('studentsCount').innerText = studentsCount;
  document.getElementById('femalesCount').textContent = femalesCount;
  document.getElementById('malesCount').appendChild(document.createTextNode(malesCount));
}

