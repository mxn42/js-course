window.onload = main;

let sortMethod;

function main() {
  students.sort(compare);
  sortMethod = compare;
  showStudents(students);
}

function sort(method) {
  if (method === sortMethod)
    students.reverse();
  else
    students.sort(method);
  sortMethod = method;
  showStudents(students);
}

function compare(a, b) {
  return groupCompare(a, b) || ageCompare(a, b) || nameCompare(a, b);
}

function groupCompare(a, b) {
  return a.group - b.group;
}

function ageCompare(a, b) {
  return a.age - b.age;
}

function nameCompare(a, b) {
  return (a.name.last).localeCompare(b.name.last) || (a.name.first).localeCompare(b.name.first);
}

function showStudents(students) {
  let tbody = document.querySelector('table tbody');
  tbody.innerHTML = '';
  for (let person of students)
    tbody.innerHTML += `
      <tr>
        <td>${ person.name.last } ${ person.name.first }
        <td>${ person.group }
        <td>${ person.age }
        <td>${ person.gender }
      </tr>
    `;
}
