
function action() {
  let text = document.getElementById('inputBox').value;

  let count = 0;
  for (let i = 0 ; i < text.length ; i += 1)
    if (text.charAt(i) === 'b')
      count += 1;

  document.getElementById('output').innerHTML = count;
}
