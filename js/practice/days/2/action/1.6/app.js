
function isPrime(a) {
  for (let i = 2; i < a; i += 1)
    if (a % i === 0)
      return false;
  return true;
}

function action() {
  let N = Number(document.getElementById('inputBox').value);

  let res = '';
  for (let i = 2; i <= N; i += 1)
    if (isPrime(i)) {
      res += i + ' ';
    }

  document.getElementById('output').innerHTML = res;
}
