
function action() {
  let N = Number(document.getElementById('inputBox').value);

  let res = '';
  for (let i = 2; i < N; i += 1)
    if (N % i === 0) {
      res += i + ' ';
    }

  document.getElementById('output').innerHTML = res;
}
