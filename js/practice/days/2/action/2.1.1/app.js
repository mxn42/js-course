
function action() {
  let str = document.getElementById('a').value;
  let a = str.split(/,\s*/).map(Number);

  let min = Infinity;
  let max = -Infinity;
  let sum = 0;
  for (let i = 0; i < a.length; i += 1) {
    if (a[i] < min)
      min = a[i];
    if (a[i] > max)
      max = a[i];
    sum += a[i];
  }
  let avg = sum / a.length;
  let res = min + ' ' + max + ' ' + sum;

  document.getElementById('output').innerHTML = res;
}
