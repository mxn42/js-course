
function action() {
  let str = document.getElementById('a').value;
  let a = str.split(/,\s*/).map(Number);

  let min = 0;
  let max = 0;
  for (let i = 1; i < a.length; i += 1) {
    if (a[i] < a[min])
      min = i;
    if (a[i] > a[max])
      max = i;
  }
  let res = a[min] + ' (' + min + ')' + '<br>' +
            a[max] + ' (' + max + ')';

  document.getElementById('output').innerHTML = res;
}
