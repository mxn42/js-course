
function compareNumbers(a, b) {
  if (Number(a) === Number(b))
    return 0;
  else if (Number(a) > Number(b))
    return 1;
  else // (a < b)
    return -1;
}

function greatCompareNumbers(a, b) {
  return a - b;
}

function action() {
  let str = document.getElementById('a').value;
  let a = str.split(/,\s*/).map(Number);

  a.sort( greatCompareNumbers );
  // a.sort((a, b) => a - b)

  let res = a.join(' | ');
  document.getElementById('output').innerHTML = res;
}
