
// http://mxn42.ru/avalon/js/nav.zip

document.addEventListener('DOMContentLoaded', main);
window.addEventListener('hashchange', nav);

function main() {
  nav();
}

function nav() {
  let hash = location.hash || '#info';
  let tabName = hash.substr(1); // info, students, groups

  for (let tab of document.querySelectorAll('.tab'))
    tab.style.display = 'none';

  let activeTab = document.querySelector('.' + tabName + '.tab');
  activeTab.style.display = 'block';

  for (let li of document.querySelectorAll('.nav.navbar-nav > li'))
    li.classList.remove('active');

  let a = document.querySelector('a[href=#"' + tabName + '"]');
  let activeLi = a.parentNode;
  activeLi.classList.add('active');
}


