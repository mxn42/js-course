
document.addEventListener('DOMContentLoaded', main);

function main() {
  showTable(elements.elements);
}

function showTable(elements) {
  let tbody = document.querySelector('table.periodical-table>tbody');
  tbody.innerHTML = '';
  for (let element of elements) {
    tbody.innerHTML += `
      <tr>
        <td>${element.number}
        <td>${element.symbol}
        <td>${element.name}
        <td>${element.atomic_mass}
    `;
  }
}
