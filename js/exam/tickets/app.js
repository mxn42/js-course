
document.addEventListener('DOMContentLoaded', function() {
  ticket1();
  ticket2();
});


/*
  ## Билет 1
  Вывести в браузер:
  - сумму всех заказов;
  - сумму, вырученную за товары, купленные по рекомендации;
  - общее количество покупателей;
  - информацию о самом дорогом заказе.
 */

function ticket1() {
  var totalSum = 0;
  for (var i = 0; i < ordersList.length; i += 1) {
    var order = ordersList[i];
    totalSum += Number(order.total);
  }

  var recommendedSum = 0;
  for (var i = 0; i < ordersList.length; i += 1) {
    var order = ordersList[i];
    recommendedSum += Number(order.recommended);
  }

  var customers = [];
  for (var i = 0; i < ordersList.length; i += 1) {
    var order = ordersList[i];
    if (customers.indexOf(order.user_id) == -1) {
      customers.push(order.user_id);
    }
  }

  var maxOrder = ordersList[0];
  for (var i = 1; i < ordersList.length; i += 1) {
    var order = ordersList[i];
    if (maxOrder.total < order.total) {
      maxOrder = order;
    }
  }

  document.getElementById('total-sum').innerHTML       = totalSum + ' руб.';
  document.getElementById('recommended-sum').innerHTML = recommendedSum + ' руб.';
  document.getElementById('customers-count').innerHTML = customers.length;
  document.getElementById('max-order').innerHTML       = '<tr>' +
      '<td>' + maxOrder.id +
      '<td>' + maxOrder.user_id +
      '<td>' + (new Date(maxOrder.timestamp * 1000).toLocaleDateString()) +
      '<td>' + maxOrder.total + ' руб.' +
      '<td>' + maxOrder.recommended  + ' руб.' +
      '<td>' + maxOrder.typical + ' руб.' ;
}

/*
  ## Билет 2
  Вывести в браузер:
  - число заказов, в которых есть товары, купленные по рекомендации;
  - сумму заказов, сделанных 28 июня 2015;
  - сумму заказов, в которых есть товары, купленные по рекомендации;
  - информацию о самом дешевом заказе.
 */

function ticket2() {
  var recommendedCount = 0;
  for (var i = 0; i < ordersList.length; i += 1) {
    var order = ordersList[i];
    if (order.recomended != 0) {
      recommendedCount += 1;
    }
  }

  var june25sum = 0;
  var june25date = new Date(2015, 5, 25);
  var june26date = new Date(2015, 5, 26);
  for (var i = 0; i < ordersList.length; i += 1) {
    var order = ordersList[i];
    var date = new Date(order.timestamp * 1000);
    if (june25date <= date && date < june26date) {
      june25sum += Number(order.total);
    }
  }

  var customers = [];
  for (var i = 0; i < ordersList.length; i += 1) {
    var order = ordersList[i];
    if (customers.indexOf(order.user_id) == -1) {
      customers.push(order.user_id);
    }
  }

  var maxOrder = ordersList[0];
  for (var i = 1; i < ordersList.length; i += 1) {
    var order = ordersList[i];
    if (maxOrder.total < order.total) {
      maxOrder = order;
    }
  }

  document.getElementById('total-sum').innerHTML       = totalSum + ' руб.';
  document.getElementById('recommended-sum').innerHTML = recommendedSum + ' руб.';
  document.getElementById('customers-count').innerHTML = customers.length;
  document.getElementById('max-order').innerHTML       = '<tr>' +
      '<td>' + maxOrder.id +
      '<td>' + maxOrder.user_id +
      '<td>' + (new Date(maxOrder.timestamp * 1000).toLocaleDateString()) +
      '<td>' + maxOrder.total + ' руб.' +
      '<td>' + maxOrder.recommended  + ' руб.' +
      '<td>' + maxOrder.typical + ' руб.' ;
}